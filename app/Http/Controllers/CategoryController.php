<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Category;
use Redirect, Response;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()){
        return Datatables::of(Category::query())->addColumn('action', function($category){
            $json = $category->toJson();
            return "
            <center>
            <div class=\"btn-group\" role=\"group\" aria-label=\"First group\">
            <button title=\" Edit $category->name \" data-id=\"$category->id\" type=\"button\" class=\"btn btn-sm btn-outline-warning btn-update\"><i class=\"fa fa-edit\"></i></button>
            <button title=\" Hapus $category->name \"  type=\"submit\" id=\"destroy\" data-id=\"$category->id\" data-name=\"$category->name\" class=\"btn btn-sm btn-outline-danger btn-destroy\"><i class=\"fa fa-trash\"></i></button>
            </div>
            </center>
            ";
        })
        ->addIndexColumn()
        ->make(true);
    }
    return view('vehicle-rent.masters.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $post   =   Category::updateOrCreate(['id' => $id],
            ['category_name' => $request->category_name]);    
        return Response::json($post);
        toastr()->success($request->name . ' Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $update  = Category::where($where)->first();
        return Response::json($update);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Category::where('id',$id)->delete();
        return Response::json($destroy);
    }
}
