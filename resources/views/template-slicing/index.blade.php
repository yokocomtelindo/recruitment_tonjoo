@extends('layouts.app')

@section('title')
Slicing Sederhana
@endsection

@section('content')
<p><div class="text-center">
	<h1 class="display-4">Layanan</h1>
</div><p>
	<div class="container">
		<div class="card-deck mb-3 text-center">
			<div class="card mb-3 shadow-sm">
				<div class="card-header">
					<h4 class="my-0 font-weight-normal">Google Adwords</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title">Rp100,000</h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>10x pertemuan</li>
						<li>Snack</li>
						<li>Makan Siang</li>
						<li>Sertifikat</li>
					</ul>
					<button type="button" class="btn btn-lg btn-block btn-outline-primary">Daftar</button>
				</div>
			</div>
			<div class="card mb-3 shadow-sm">
				<div class="card-header">
					<h4 class="my-0 font-weight-normal">Facebook Ads</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title">Rp175,000</h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>10x pertemuan</li>
						<li>Snack</li>
						<li>Makan Siang</li>
						<li>Sertifikat</li>
					</ul>
					<button type="button" class="btn btn-lg btn-block btn-outline-primary">Daftar</button>
				</div>
			</div>
			<div class="card mb-3 shadow-sm">
				<div class="card-header">
					<h4 class="my-0 font-weight-normal">SEO</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title">Rp200,000</h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>10x pertemuan</li>
						<li>Snack</li>
						<li>Makan Siang</li>
						<li>Sertifikat</li>
					</ul>
					<button type="button" class="btn btn-lg btn-block btn-outline-primary">Daftar</button>
				</div>
			</div>
			<div class="card mb-3 shadow-sm">
				<div class="card-header">
					<h4 class="my-0 font-weight-normal">Training</h4>
				</div>
				<div class="card-body">
					<h1 class="card-title pricing-card-title">Rp150,000</h1>
					<ul class="list-unstyled mt-3 mb-4">
						<li>10x pertemuan</li>
						<li>Snack</li>
						<li>Makan Siang</li>
						<li>Sertifikat</li>
					</ul>
					<button type="button" class="btn btn-lg btn-block btn-outline-primary">Daftar</button>
				</div>
			</div>
		</div>

		<p><div class="text-center">
			<h1 class="display-4">Blog</h1>
		</div><p>

		<div class="row featurette">
			<div class="col-md-9 order-md-2">
				<h2 class="featurette-heading">CONTENT TITLE</h2>
				<p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
			</div>
			<div class="col-md-3 order-md-1">
				<img src="{{asset('images/150.png')}}">
			</div>
		</div>
		<br>

		<div class="row featurette">
			<div class="col-md-9">
				<h2 class="featurette-heading">CONTENT TITLE</h2>
				<p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
			</div>
			<div class="col-md-3">
				<img src="{{asset('images/150.png')}}">
			</div>
		</div>
	

		
		<div class="row featurette">
			<div class="col-md-9 order-md-2">
				<h2 class="featurette-heading">CONTENT TITLE</h2>
				<p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
			</div>
			<div class="col-md-3 order-md-1">
				<img src="{{asset('images/150.png')}}">
			</div>
		</div>

	</div>
@endsection