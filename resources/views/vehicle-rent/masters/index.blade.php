<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include("layouts.head")
</head>
<body>
    @include("layouts.rent@nav")
    <div class="flex-center position-ref full-height">
        <div class="container">
            <button type="button" class="btn btn-primary" id="category_btn"><i class="fa fa-plus"></i>Tambahkan Kategori</button><p>
                <div class="row">
                    <div class="col-md-12" id="content">
                    <div class="card">
                        <div class="card-header text-center">TES KEMAMPUAN PHP | TRANSAKSI</div>
                        <div class="card-body">
                            <div class="container bootstrap snippet">
                                <div class="container">
                                    <div class="table-responsive">
                                        <table id="category_table" class="table m-table m-table--head-bg-brand">
                                            <thead>
                                                <tr>
                                                    <th width="10px">No</th>
                                                    <th>Nama Kategori</th>
                                                    <th><center>Aksi</center></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include ('vehicle-rent.masters.store')
            </div>
        </div>
    </div>
</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="{{asset('js/init.js')}}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //get datatable
    $(document).ready(function(){
        $('#category_btn').click(function(){
            var form    = $('#category_form');
            var view    = $('#content');

            if(form.is(':visible') && $('#btn__cu').val()=="create-category"){
                $('#btn__close').click();
                return;
            }
            $('#title').html('<i class="fa flaticon-plus" id="icon"></i> Tambah Kategori');
            $('#btn__cu').val("create-category");
            $('#form__action').trigger("reset");
            $('#id').val('');

            view.removeClass("col-md-12").addClass('col-md-8');
            form.show().removeClass('fadeOut').addClass('slideLeft fadeIn');
            $(this).removeClass("btn-primary").addClass('btn-danger');
            $(this).html(' <i class="fa flaticon-close" id="icon"></i> Tutup');
        });

        $('#btn__close').click(function(){
        $('#category_form').hide();
        $('#content').removeClass("col-md-8").addClass('col-md-12');
        $('#category_btn').removeClass("btn-danger").addClass('btn-primary');
        $('#category_btn').html(' <i class="fa flaticon-plus" id="icon"></i> Tambahkan Kategori');
    });
        $('#category_table')
        .DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('kategori.index') !!}',
            columns: [
            { data: 'DT_RowIndex','orderable':false,'searchable':false},
            { data: 'category_name', name: 'category_name' },
            { data: 'action', name: 'action' },
            ],
        });

        //store
        if ($("#form__action").length > 0) {
            $("#form__action").validate({
                rules: {
                    category_name: {
                        required: true,
                        maxlength: 50
                    },

                },
                messages: {
                    category_name: {
                        required: "Kategori wajib diisi",
                        maxlength: "Panjang maksimal karakter 50."
                    },
                },
                submitHandler: function(form) {
                    var btn     = $('#btn__cu');
                    var actionType = btn.val();
                    btn.html('Mengirim..');
                    $.ajax({
                        data: $('#form__action').serialize(),
                        url: '{{route("kategori.store")}}',
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            $('#form__action').trigger("reset");
                            btn.html('<i class="fa fa-save" id="icon"></i> Simpan');
                            var oTable = $('#category_table').dataTable();
                            oTable.fnDraw(false);
                            toastr.success('Data berhasil disimpan.','Sukses!');
                            if (actionType == 'update-category') {
                                $('#btn__close').click();
                            }
                        },
                        error: function (xhr, statusText, errorThrown) {
                            if (xhr.status == 500) {
                                toastr.error('Kategori tidak boleh sama' ,'Error!');
                            }
                        }
                    });
                }
            })
        }
        //get update
        $('body').on('click', '.btn-update', function () {
            if ($('#category_btn').hasClass('btn-danger')) {
                $('#category_btn').removeClass('btn-danger').addClass("btn-primary");
                $('#category_btn').html(' <i class="fa flaticon-plus" id="icon"></i> Tambahkan Kategori');
            }
            var id = $(this).data('id');
            $.get('kategori/' + id +'/edit', function (data) {
                var form    = $('#category_form');
                var view    = $('#content');
                
                if(form.is(':visible') && $('#btn__cu').val()=="update-category"){
                    view.removeClass("col-lg-8").addClass('col-lg-12');
                    form.hide();
                    return;
                }
                $('#title').html('<i class="fa flaticon-edit" id="icon"></i> Edit Kategori');
                $('#btn__cu').val("update-category");
                $('#id').val(data.id);
                $('#category_name').val(data.category_name);

                view.removeClass("col-md-12").addClass('col-md-8');
                form.show().removeClass('fadeOut').addClass('slideLeft fadeIn');
            })
        });

        //destroy
        $('body').on('click', '#destroy', function () {
            var id = $(this).data('id');
            var category_name = $(this).data('category_name');
            var result = confirm('Yakin ingin menghapus '+category_name+'?');
            if (result==true) {
                $.ajax({
                    type: "DELETE",
                    url:"kategori/"+id,
                    success: function (data) {
                        var oTable = $('#category_table').dataTable(); 
                        oTable.fnDraw(false);
                    },
                }).done(function(data){
                    var oTable = $('#category_table').dataTable(); 
                    oTable.fnDraw(false);
                    toastr.error(''+category_name+' berhasil dihapus.','Sukses!');
                });
            } else {
                return false;
            }
        });
    })
</script>
