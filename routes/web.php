<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/template-slicing', function () {
    return view('template-slicing.index');
})->name('template-slicing');

Route::get('/jquery', function () {
    return view('jquery.index');
})->name('jquery.index');;


Route::resource('/crud-pegawai','EmployeeController');

Route::prefix('transaksi/kategori')->group(function () {
    Route::get('/','CategoryController@index')->name('kategori.index');
    Route::post('/','CategoryController@store')->name('kategori.store');
    Route::delete('/{id}','CategoryController@destroy')->name('kategori.destroy');
    Route::get('/{id}/edit','CategoryController@edit')->name('kategori.edit');
});


Route::prefix('transaksi')->group(function () {
    Route::get('/','TransactionController@index')->name('transaksi.index');
    Route::post('/','TransactionController@store')->name('transaksi.store');
    Route::delete('/{id}','TransactionController@destroy')->name('transaksi.destroy');
    Route::get('/{id}/edit','TransactionController@edit')->name('transaksi.edit');



});
