<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include("layouts.head")
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header text-center">RECRUITMENT PT.TONJOO GAGAS TEKNOLOGI</div>
                        <div class="card-body">
                            <div class="container bootstrap snippet">
                                <div class="container">
                                    <div class="card-deck mb-3 text-center">

                                        <div class="card mb-4 shadow-sm">
                                            <div class="card-header">
                                                <h4 class="my-0 font-weight-normal">Tes HTML CSS</h4>
                                            </div>
                                            <div class="card-body">
                                                <a href="{{route('template-slicing')}}" target="_blank"><button type="button" class="btn btn-lg btn-block btn-outline-primary"><i class="fa fa-arrow-right"></i></button></a>
                                            </div>
                                        </div>

                                        <div class="card mb-4 shadow-sm">
                                            <div class="card-header">
                                                <h4 class="my-0 font-weight-normal">Tes Kemampuan PHP</h4>
                                            </div>
                                            <div class="card-body">
                                                <a href="{{route('crud-pegawai.index')}}" target="_blank"><button type="button" class="btn btn-lg btn-block btn-outline-primary"><i class="fa fa-arrow-right"></i></button></a>
                                            </div>
                                        </div>

                                        <div class="card mb-4 shadow-sm">
                                            <div class="card-header">
                                                <h4 class="my-0 font-weight-normal">Tes Kemampuan Jquery</h4>
                                            </div>
                                            <div class="card-body">
                                                <a href="{{route('jquery.index')}}" target="_blank"><button type="button" class="btn btn-lg btn-block btn-outline-primary"><i class="fa fa-arrow-right"></i></button></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card mb-12 shadow-sm">
                                        <div class="card-header">
                                            <h4 class="my-0 font-weight-normal"><center>Transaksi Rental Mobil</h4>
                                        </div>
                                        <div class="card-body">
                                            <a href="{{route('transaksi.index')}}" target="_blank"><button type="button" class="btn btn-lg btn-block btn-outline-primary"><i class="fa fa-arrow-right"></i></button></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
