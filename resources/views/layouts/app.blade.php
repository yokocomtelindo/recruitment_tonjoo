<!DOCTYPE html>
<html lang="en" >
<head>	
	@include("layouts.head")
	@stack('style')

</head>
<body>
	@include("layouts.head@nav")
	@include("layouts.head@image-head")

	@section("content")
	@show
</body>
@stack('script')
</html>
