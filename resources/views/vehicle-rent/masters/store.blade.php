
<div class="col-md-4" id="category_form" style="display:none;">
	<div class="card">
		<div class="card-header text-center" id="title"></div>
		<div class="card-body">
			<div class="container bootstrap snippet">
				<div class="container">
					<form id="form__action" name="form__action">
						<div class="kt-portlet__body">
							<input type="hidden" name="id" id="id">
							<div class="form-group m-form__group">
								<label>Nama Kategori</label>
								<input name="category_name" id="category_name" placeholder="Kategori" class="form-control m-input " type="text" required="">
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="form-group m-form__group  ">
								<button type="button" id="btn__close" class="btn btn-danger float-left"><i class="flaticon-close"></i> Batal</button>
								<button  type="submit" id="btn__cu" class="btn btn-primary float-right" value="create-categpry" ><i class="fa fa-save"></i> Simpan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>