<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<script src="{{asset('js/summernote-image-attributes.js')}}"></script>
<script>
	$(document).ready(function() {
		$('#content').summernote({
			placeholder: 'Konten Email',
			height    : 200,
			tabsize: 3,
			dialogsInBody: true,
			disableDragAndDrop: true,
			popatmouse: false,
			toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture', 'link', 'video','hr']],
                ['misc', ['codeview','fullscreen']]
                ],
                popover: {
                	image: [
                	['custom', ['imageAttributes']],
                	['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                	['float', ['floatLeft', 'floatRight', 'floatNone']],
                	['remove', ['removeMedia']]
                	]
                },
                imageAttributes:{
                	icon:'<i class="note-icon-pencil"/>',
                removeEmpty:false, // true = remove attributes | false = leave empty if present
                disableUpload: true // true = don't display Upload Options | Display Upload Options
            },
            callbacks: {
            	onInit: function() {
            		var c = $('[data-original-title="Remove Image"]').click();
            		
            		
            	}
            }
        }); 
	});
</script>