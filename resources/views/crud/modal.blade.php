<div class="modal fade" id="employees_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Tambah Pegawai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form__action" name="form__action">
                <div id="employees_form" class="modal-body">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" name="id" id="id">
                            <label>Nama</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama" required>
                        </div>
                        <div class="form-group">
                            <label>Umur</label>
                            <input type="text" class="form-control" id="age" name="age" placeholder="Masukan Umur" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn_store" value="employees_store" type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>