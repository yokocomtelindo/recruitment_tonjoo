<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include("layouts.head")
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header text-center">RECRUITMENT PT.TONJOO GAGAS TEKNOLOGI</div>
                        <div class="card-body">
                            <div class="container bootstrap snippet">
                                <div class="container">
                                    <div class="control-group" id="fields">
                                        <div class="controls"> 
                                            <form>
                                                <div class="entry input-group col-xs-3">
                                                    <input class="form-control" name="fields[]" type="text">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-primary btn-add" type="button">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script type="text/javascript">
    $(document).on('click', '.btn-add', function(e) {
        e.preventDefault();
        var controlForm = $('.controls form:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<i class="fa fa-minus"></i>');

    }).on('click', '.btn-remove', function(e)
    {
        $(this).parents('.entry:first').remove();
        e.preventDefault();
        return false;
    });
</script>
