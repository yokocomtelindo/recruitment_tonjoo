
<div class="col-md-4" id="transaction_form" style="display:none;">
	<div class="card">
		<div class="card-header text-center" id="title"></div>
		<div class="card-body">
			<div class="container bootstrap snippet">
				<div class="container">
					<form id="form__action" name="form__action">
						<div class="kt-portlet__body">
							<input type="hidden" name="id" id="id">
							<div class="form-group m-form__group">
								<label>Deskripsi</label>
								<input name="description" id="description"  class="form-control m-input " type="text" required="">
							</div>

							<div class="form-group m-form__group">
								<label>Kode Transaksi</label>
								<input name="code" id="code"  class="form-control m-input " type="text" required="">
							</div>

							<div class="form-group m-form__group">
								<label>Rate Euro</label>
								<input name="rate_euro" id="rate_euro"  class="form-control m-input " type="text" required="">
							</div>
							<div class="form-group m-form__group">
								<label>Date paid</label>
								<input name="date_paid" id="date_paid"  class="form-control m-input " type="text" required="">
							</div>
							<div class="form-group m-form__group">
								<label>Kategori</label>
								<select class="form-control m-input" name="category_id" id="category_id">
									@foreach ($categories as $get)
									<option data-name="category_id" value="{{$get->id}}">{{$get->category_name}}</option>
									@endforeach
								</select>
								<div class="form-group m-form__group">
									<label>Nama Transaksi</label>
									<input name="transaction_name" id="transaction_name"  class="form-control m-input " type="text" required="">
								</div>
								<div class="form-group m-form__group">
									<label>Nominal</label>
									<input name="nominal" id="nominal"  class="form-control m-input " type="text" required="">
								</div>
							</div>
						</div>
						<div class="kt-portlet__foot">
							<div class="form-group m-form__group  ">
								<button type="button" id="btn__close" class="btn btn-danger float-left"><i class="flaticon-close"></i> Batal</button>
								<button  type="submit" id="btn__cu" class="btn btn-primary float-right" value="create-transaction" ><i class="fa fa-save"></i> Simpan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>