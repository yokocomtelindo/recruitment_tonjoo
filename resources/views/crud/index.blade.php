<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include("layouts.head")
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header text-center">TES KEMAMPUAN PHP | CRUD DATA PEGAWAI</div>
                        <div class="card-body">
                            <div class="container bootstrap snippet">
                                <div class="container">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#employees_modal"><i class="fa fa-plus"></i>Tambahkan Pegawai</button><p>
                                    <div class="table-responsive">
                                        <table id="employees_table" class="table m-table m-table--head-bg-brand " >
                                            <thead>
                                                <tr>
                                                    <th width="10px">No</th>
                                                    <th>Nama</th>
                                                    <th>Umur</th>
                                                    <th><center>Aksi</center></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('crud.modal')
</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="{{asset('js/init.js')}}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //get
    $(document).ready(function(){
        $('#employees_table')
        .DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('crud-pegawai.index') !!}',
            columns: [
            { data: 'DT_RowIndex','orderable':false,'searchable':false},
            { data: 'name', name: 'name' },
            { data: 'age', name: 'age' },
            { data: 'action', name: 'action' },
            ],
        });

        //store
        if ($("#form__action").length > 0) {
            $("#form__action").validate({
                rules: {
                    name: {
                        required: true,
                        maxlength: 50
                    },

                    age: {
                        required: true,
                        digits:true,
                    },    
                },
                messages: {
                    name: {
                        required: "Nama wajib diisi",
                        maxlength: "Panjang maksimal karakter 50."
                    },
                    age: {
                        required: "Umur wajib diisi",
                        digits: "Hanya input angka."
                    },
                },
                submitHandler: function(form) {
                    var btn     = $('#btn__store');
                    var actionType = btn.val();
                    btn.html('Mengirim..');
                    $.ajax({
                        data: $('#form__action').serialize(),
                        url: '{{route("crud-pegawai.store")}}',
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            $('#form__action').trigger("reset");
                            btn.html('<i class="fa fa-save" id="icon"></i> Simpan');
                            var oTable = $('#employees_table').dataTable();
                            oTable.fnDraw(false);
                            toastr.success('Data berhasil disimpan.','Sukses!');
                            if (actionType == 'update-employees') {
                                $('#btn__close').click();
                            }
                        },
                        error: function (xhr, statusText, errorThrown) {
                            if (xhr.status == 500) {
                                toastr.error('Nama tidak boleh sama' ,'Error!');
                            }
                        }
                    });
                }
            })
        }
        //get update
        $('body').on('click', '.btn-update', function () {
        var id = $(this).data('id');
        var name = $(this).data('name');
        $.get("{{ route('crud-pegawai.index') }}" +'/' + id +'/edit', function (data) {
            $('#modal-title').html('Edit Pegawai');
            $('#btn_store').val("update-employees");
            $('#employees_modal').modal('show');
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#age').val(data.age);
            })
        });

        //destroy
        $('body').on('click', '#destroy', function () {
            var id = $(this).data('id');
            var name = $(this).data('name');
            var result = confirm('Yakin ingin menghapus '+name+'?');
            if (result==true) {
                $.ajax({
                    type: "DELETE",
                    url:"crud-pegawai/"+id,
                    success: function (data) {
                        var oTable = $('#employees_table').dataTable(); 
                        oTable.fnDraw(false);
                    },
                }).done(function(data){
                    var oTable = $('#employees_table').dataTable(); 
                    oTable.fnDraw(false);
                    toastr.error(''+name+' berhasil dihapus.','Sukses!');
                });
            } else {
                return false;
            }
        });
    })
</script>
