<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	public $timestamps = true;
	protected $fillable = [
		'description',
		'code',
		'rate_euro',
		'date_paid',
		'transaction_name',
		'nominal',
		'category_id',
		'created_at',

	];

	public function category(){
		return $this->hasOne('App\Model\Category');
	}
}
