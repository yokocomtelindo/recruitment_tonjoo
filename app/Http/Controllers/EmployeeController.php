<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Employee;
use Redirect, Response;
use Yajra\DataTables\DataTables;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     if(request()->ajax()){
        return Datatables::of(Employee::query())->addColumn('action', function($employee){
            $json = $employee->toJson();
            return "
            <center>
            <div class=\"btn-group\" role=\"group\" aria-label=\"First group\">
            <button title=\" Edit $employee->name \" data-id=\"$employee->id\" type=\"button\" class=\"btn btn-sm btn-outline-warning btn-update\"><i class=\"fa fa-edit\"></i></button>
            <button title=\" Hapus $employee->name \"  type=\"submit\" id=\"destroy\" data-id=\"$employee->id\" data-name=\"$employee->name\" class=\"btn btn-sm btn-outline-danger btn-destroy\"><i class=\"fa fa-trash\"></i></button>
            </div>
            </center>
            ";
        })
        ->addIndexColumn()
        ->make(true);
    }
    return view('crud.index');
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $post   =   Employee::updateOrCreate(['id' => $id],
            ['name' => $request->name, 'age' => $request->age]);    
        return Response::json($post);
        toastr()->success($request->name . ' Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $update  = Employee::where($where)->first();
        return Response::json($update);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Employee::where('id',$id)->delete();
        return Response::json($destroy);
    }
}
