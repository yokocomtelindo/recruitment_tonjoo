<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include("layouts.head")
</head>
<body>
    @include("layouts.rent@nav")
    <div class="flex-center position-ref full-height">
        <div class="container">
            <button type="button" class="btn btn-primary" id="transaction_btn"><i class="fa fa-plus"></i>Tambahkan Transaksi</button><p>
                <div class="row">
                    <div class="col-md-12" id="content">

                    <div class="card">
                        <div class="card-header text-center">TES KEMAMPUAN PHP | TRANSAKSI</div>
                        <div class="card-body">
                        <div class="col-lg-3">
                            <label>Periode:</label>
                            <div class="input-daterange input-group" id="m_datepicker">
                                <input type="text" class="form-control m-input" autocomplete="off" id="filterPeriode" placeholder="">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <label>&nbsp;</label><br>
                            <button class="btn btn-primary" id="m_search">
                                <span>Cari</span>
                            </button>
                        </div>
                        <br>

                            <div class="container bootstrap snippet">
                                <div class="container">
                                    <div class="table-responsive">
                                        <table id="transaction_table" class="table m-table m-table--head-bg-brand">
                                            <thead>
                                                <tr>
                                                    <th width="10px">No</th>
                                                    <th>Deskripsi</th>
                                                    <th>Kode</th>
                                                    <th>Rate euro</th>
                                                    <th>Tanggal</th>
                                                    <th>Nama Transaksi</th>
                                                    <th>Nominal</th>
                                                    <th>Kategori</th>
                                                    <th>Created_at</th>

                                                    <th><center>Aksi</center></th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include ('vehicle-rent.transaction.store')
            </div>
        </div>
    </div>
</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="{{asset('js/init.js')}}"></script>
<script src="http://email.syahdinaland.com/js/bootstrap-datetimepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
<script>

    $( function() {
        $("#date_paid").datepicker({
            format: 'yyyy/mm/dd'
        });
    });
</script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //get datatable
    $(document).ready(function(){

        var parseDateRange = function(element){
        var el = $(element)
        var range = el.data('daterangepicker')
            if(el.val() == ''){
                return null
            }
            return [range.startDate.format('YYYY-MM-DD'), range.endDate.format('YYYY-MM-DD')]
        }
    // var perBandingan = prompta
            var onApplyDateRange = function(ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            }

            var onClearDateRange = function(ev, picker) {
                $(this).val('');
            }

            $('#filterPeriode').on('cancel.daterangepicker', onClearDateRange).on('apply.daterangepicker', onApplyDateRange).daterangepicker({autoUpdateInput: false,locale: {cancelLabel: 'Clear'}});

            var getFilter = function(){
                return {
                    'periode': parseDateRange('#filterPeriode'),
                }
            }
            var btnSearch = $('#m_search')
            btnSearch.on('click', function(){
                tableTransaction.draw()
            })  


        $('#transaction_btn').click(function(){
            var form    = $('#transaction_form');
            var view    = $('#content');

            if(form.is(':visible') && $('#btn__cu').val()=="create-transaction"){
                $('#btn__close').click();
                return;
            }
            $('#title').html('<i class="fa flaticon-plus" id="icon"></i> Tambah Transaksi');
            $('#btn__cu').val("create-transaction");
            $('#form__action').trigger("reset");
            $('#id').val('');

            view.removeClass("col-md-12").addClass('col-md-8');
            form.show().removeClass('fadeOut').addClass('slideLeft fadeIn');
            $(this).removeClass("btn-primary").addClass('btn-danger');
            $(this).html(' <i class="fa flaticon-close" id="icon"></i> Tutup');
        });

        $('#btn__close').click(function(){
        $('#category_form').hide();
        $('#content').removeClass("col-md-8").addClass('col-md-12');
        $('#category_btn').removeClass("btn-danger").addClass('btn-primary');
        $('#category_btn').html(' <i class="fa flaticon-plus" id="icon"></i> Tambahkan Kategori');
    });
            window.tableTransaction = $('#transaction_table').DataTable({
            processing: true,
            serverSide: true,
            searching: false,
            dom: 'Bfrtip',
            buttons: [
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', customize : function(doc) {doc.pageMargins = [10, 10, 10,10 ]; }, footer: true }
            ],
            ajax: {
            url:'{!! route('transaksi.index') !!}',
            data: function(data){
                data.filters = getFilter()
                }
            },

            columns: [
            { data: 'DT_RowIndex','orderable':false,'searchable':false},
            { data: 'description', name: 'description' },
            { data: 'code', name: 'code' },
            { data: 'rate_euro', name: 'rate_euro' },
            { data: 'date_paid', name: 'date_paid' },
            { data: 'transaction_name', name: 'transaction_name' },
            { data: 'nominal', name: 'nominal' },
            { data: 'category_name', name: 'categories.category_name' },
            { data: 'created_at', name: 'created_at' },
            { data: 'action', name: 'action' },
            ],
        });

        //store
        if ($("#form__action").length > 0) {
            $("#form__action").validate({
                rules: {
                    description: {
                        required: true,
                        maxlength: 50
                    },
                    code: {
                        required: true,
                    },
                    rate_euro: {
                        required: true,
                        digits: true
                    },
                    date_paid: {
                        required: true,
                    },
                    transaction_name: {
                        required: true,
                    },
                    nominal: {
                        required: true,
                        digits: true,
                    },
                },
                messages: {
                    description: {
                        required: "Deskripsi wajib diisi",
                        maxlength: "Panjang maksimal karakter 50."
                    },
                    code: {
                        required: "Kode wajib diisi"
                    },
                    rate_euro: {
                        required: "Rate euro wajib diisi",
                        digits: "Hanya boleh isi angka"
                    },
                    transaction_name: {
                        required: "Wajib diisi"
                    },
                    nominal: {
                        required: "Wajib diisi",
                        digits: "Hanya Boleh input angka"
                    },
                },
                submitHandler: function(form) {
                    var btn     = $('#btn__cu');
                    var actionType = btn.val();
                    btn.html('Mengirim..');
                    $.ajax({
                        data: $('#form__action').serialize(),
                        url: '{{route("transaksi.store")}}',
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            $('#form__action').trigger("reset");
                            btn.html('<i class="fa fa-save" id="icon"></i> Simpan');
                            var oTable = $('#transaction_table').dataTable();
                            oTable.fnDraw(false);
                            toastr.success('Data berhasil disimpan.','Sukses!');
                            if (actionType == 'update-transaction') {
                                $('#btn__close').click();
                            }
                        },
                        error: function (xhr, statusText, errorThrown) {
                            if (xhr.status == 500) {
                                toastr.error('Transaksi tidak boleh sama' ,'Error!');
                            }
                        }
                    });
                }
            })
        }
        //get update
        $('body').on('click', '.btn-update', function () {
            if ($('#transaction_btn').hasClass('btn-danger')) {
                $('#transaction_btn').removeClass('btn-danger').addClass("btn-primary");
                $('#transaction_btn').html(' <i class="fa flaticon-plus" id="icon"></i> Tambahkan Transaksi');
            }
            var id = $(this).data('id');
            $.get('transaksi/' + id +'/edit', function (data) {
                var form    = $('#transaction_form');
                var view    = $('#content');
                
                if(form.is(':visible') && $('#btn__cu').val()=="update-transaction"){
                    view.removeClass("col-lg-8").addClass('col-lg-12');
                    form.hide();
                    return;
                }

                $('#title').html('<i class="fa flaticon-edit" id="icon"></i> Edit Transaksi');
                $('#btn__cu').val("update-transaction");
                $('#id').val(data.id);
                $('#description').val(data.description);
                $('#code').val(data.code);
                $('#rate_euro').val(data.rate_euro);
                $('#date_paid').val(data.date_paid);
                $('#transaction_name').val(data.transaction_name);
                $('#nominal').val(data.nominal);
                view.removeClass("col-md-12").addClass('col-md-8');
                form.show().removeClass('fadeOut').addClass('slideLeft fadeIn');
            })
        });

        //destroy
        $('body').on('click', '#destroy', function () {
            var id = $(this).data('id');
            var result = confirm('Yakin ingin menghapus ?');
            if (result==true) {
                $.ajax({
                    type: "DELETE",
                    url:"transaksi/"+id,
                    success: function (data) {
                        var oTable = $('#transaction_table').dataTable(); 
                        oTable.fnDraw(false);
                    },
                }).done(function(data){
                    var oTable = $('#transaction_table').dataTable(); 
                    oTable.fnDraw(false);
                    toastr.error('berhasil dihapus.','Sukses!');
                });
            } else {
                return false;
            }
        });
    })
</script>
