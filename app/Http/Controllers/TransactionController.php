<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\Transaction;
use App\Model\Category;
use Redirect, Response;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::all();

        $query = DB::table('transactions')->join('categories', 'transactions.category_id', '=', 'categories.id')->select('transactions.*', 'categories.category_name');

        if($periode = $request->filters['periode']){
            $query = $query->whereBetween('transactions.date_paid', [
                \Carbon\Carbon::createFromFormat('Y/m/d', $periode[0]),
                \Carbon\Carbon::createFromFormat('Y/m/d', $periode[1]),
            ]);
        }
        if(request()->ajax()){
            return Datatables::of($query)->addColumn('action', function($transaction){
                return "
                <center>
                <div class=\"btn-group\" role=\"group\" aria-label=\"First group\">
                <button title=\" Edit $transaction->transaction_name \" data-id=\"$transaction->id\" type=\"button\" class=\"btn btn-sm btn-outline-warning btn-update\"><i class=\"fa fa-edit\"></i></button>
                <button title=\" Hapus $transaction->transaction_name \"  type=\"submit\" id=\"destroy\" data-id=\"$transaction->id\" data-name=\"$transaction->transaction_name\" class=\"btn btn-sm btn-outline-danger btn-destroy\"><i class=\"fa fa-trash\"></i></button>
                </div>
                </center>
                ";
            })
            ->addIndexColumn()
            ->make(true);
        }
        return view('vehicle-rent.transaction.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $post   =   Transaction::updateOrCreate(['id' => $id],
            ['description' => $request->description, 'code' => $request->code, 'rate_euro' => $request->rate_euro, 'date_paid' => $request->date_paid, 'transaction_name' => $request->transaction_name, 'nominal' => $request->nominal, 'category_id' => $request->category_id]);    
        return Response::json($post);
        toastr()->success($request->transaction_name . ' Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $update  = Transaction::where($where)->first();
        return Response::json($update);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Transaction::where('id',$id)->delete();
        return Response::json($destroy);
    }
}
